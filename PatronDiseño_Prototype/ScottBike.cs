﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronDiseño_Prototype
{
    class ScottBike : ConstructorBicicleta
    {
        public override ConstructorBicicleta Clonacion()
        {
            return (ScottBike)this.MemberwiseClone(); // Método antes mencionado; Este método devolverá un objeto de tipo ScottBike().
        }

        public override string VerCatalogo() => "El módelo de la bicicleta es: " + ModeloBici + "\nEl color de la bicicleta es: " + ColorBici + "\nEl precio es : $" + PrecioBici;

    }
}
