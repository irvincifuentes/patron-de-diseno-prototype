﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronDiseño_Prototype
{
    // Definimos una clase abstracta que contendrá las propiedades de las bicicletas, así como los métodos que
    // harán posible la clonación y visualizar el catálogo disponible.
    public abstract class ConstructorBicicleta 
    {
        public string ModeloBici;
        public string ColorBici;
        public decimal PrecioBici;

        public string Modelo { set => ModeloBici = value; }
        public string Color { set => ColorBici = value; }
        public int Precio { set => PrecioBici = value; }


        // Gracias a un método de Visual Studio es posible realizar un clon de un objeto mediante MemberwiseClone()

        public abstract ConstructorBicicleta Clonacion();
        public abstract string VerCatalogo();

    }


    
}
