﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronDiseño_Prototype
{
    class SpecializedBike : ConstructorBicicleta
    {
        public override ConstructorBicicleta Clonacion()
        {
            return (SpecializedBike)this.MemberwiseClone(); // Método antes mencionado; Este método devolverá un objeto de tipo SpecializedBike().
        }

        public override string VerCatalogo() => "El módelo de la bicicleta es: " + ModeloBici + "\nEl color de la bicicleta es: " + ColorBici + "\nEl precio es : $" + PrecioBici;

    }
}
