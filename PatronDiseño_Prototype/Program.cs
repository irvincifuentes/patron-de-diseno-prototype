﻿using System;

namespace PatronDiseño_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instanciamos un objeto de la clase abstracta para cada marca de bicicleta.
            ConstructorBicicleta ClonScottBike = new ScottBike();
            ConstructorBicicleta ClonSpecializedBike = new SpecializedBike();
            ConstructorBicicleta ClonTrekBike = new TrekBike();

            Console.WriteLine("▬▬▬▬▬ Catálogo Bicicletas SCOTT ▬▬▬▬▬\n");
            // Por motivos de ver en clase se mostrará primero el objeto base, sin clonar y sin valores asignados.
            Console.WriteLine(ClonScottBike.VerCatalogo());
            Console.WriteLine("----------------------");

            //Ahora se clonará un objeto de la clase ConstructorBicicleta() con sus propiedades y les daremos valores.
            ConstructorBicicleta FamiliaEmonda = ClonScottBike.Clonacion();
            FamiliaEmonda.Modelo = "Émonda SL 6 Disc";
            FamiliaEmonda.Color = "Rojo";
            FamiliaEmonda.Precio = 3699;
            Console.WriteLine(FamiliaEmonda.VerCatalogo());
            Console.WriteLine("----------------------");
            //Así mismo clonaré otro objeto para dar otros valores.
            ConstructorBicicleta FamiliaMadone = ClonScottBike.Clonacion();
            FamiliaMadone.Modelo = "Madone SL 7 eTap";
            FamiliaMadone.Color = "Azul";
            FamiliaMadone.Precio = 7299;
            Console.WriteLine(FamiliaMadone.VerCatalogo());
            Console.WriteLine("----------------------");

            // Solo se añadieron bicicletas de una sola marca: Añadir en clases las siguientes:
            // • Bicicleta Specialized : Familia Jett ; Modelo: Jett20 ; Color: Rojo ; Precio: $470.00
            // • Bicicleta Trek : Familia Spark ; Modelo: Spark 900 Ultimate EVO AXS ; Color: Negro ; Precio: $13,209.00


            Console.WriteLine("▬▬▬▬▬ Catálogo Bicicletas SPECIALIZED ▬▬▬▬▬\n");




            Console.WriteLine("▬▬▬▬▬ Catálogo Bicicletas TREK ▬▬▬▬▬\n");
        }
    }
}
