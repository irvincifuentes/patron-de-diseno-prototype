﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronDiseño_Prototype
{
    class TrekBike : ConstructorBicicleta
    {
        public override ConstructorBicicleta Clonacion() // Método antes mencionado; Este método devolverá un objeto de tipo TrekBike().
        {
            return (TrekBike)this.MemberwiseClone();
        }

        public override string VerCatalogo() => "El módelo de la bicicleta es: " + ModeloBici + "\nEl color de la bicicleta es: " + ColorBici + "\nEl precio es : $" + PrecioBici;

    }
}
